trigger DFProduct on Product2 (before insert,after insert, before update,after update)  { 
	DemandFarmSettings__c dfs = DFCommonUtils.getDFSettings();
	if (dfs == null || dfs.DisableTriggers__c || !DFProductTriggerHandler.isEnable()) {
		return;
	}
	DFProductTriggerHandler handler = new DFProductTriggerHandler();
	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			handler.onBeforeInsert(trigger.new);
		} else if (Trigger.isUpdate) {
			DFProductTriggerHandler.increaseBRCount();
			if (DFProductTriggerHandler.isBTriggerActive()) {
				handler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			//handler.onBeforeDelete(trigger.old);
		}
	} else if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			handler.onAfterInsert(trigger.newMap);
		} else if (Trigger.isUpdate) {
			DFProductTriggerHandler.increaseARCount();
			if (DFProductTriggerHandler.isATriggerActive()) {
				handler.onAfterUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			//handler.onAfterDelete(trigger.old);
		}
	}
}