trigger DFOpportunityLineItem on OpportunityLineItem(before insert, after insert, before update, after update,before delete,after delete) {
	DemandFarmSettings__c dfs = DFCommonUtils.getDFSettings();
	if (dfs == null || dfs.DisableTriggers__c || !DFOpportunityLineItemTriggerHandler.isEnable()) {
		return;
	}

	DFOpportunityLineItemTriggerHandler handler = new DFOpportunityLineItemTriggerHandler();
	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			handler.onBeforeInsert(trigger.new);
		} else if (Trigger.isUpdate) {
			DFOpportunityLineItemTriggerHandler.increaseBRCount();
			if (DFOpportunityLineItemTriggerHandler.isBTriggerActive()) {
				handler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			handler.onBeforeDelete(trigger.old);
		}
	} else if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			handler.onAfterInsert(trigger.newMap);
		} else if (Trigger.isUpdate) {
			DFOpportunityLineItemTriggerHandler.increaseARCount();
			if (DFOpportunityLineItemTriggerHandler.isATriggerActive()) {
				handler.onAfterUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			handler.onAfterDelete(trigger.old);
		}
	}
}