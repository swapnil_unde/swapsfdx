trigger DFEngagementLineItem on dFarm__EngagementLineItem__c(before insert, before update, after insert, after update,before delete,after delete) {
	DemandFarmSettings__c dfs = DFCommonUtils.getDFSettings();
	if (dfs == null || dfs.DisableTriggers__c || !DFEngagementLineItemTriggerHandler.isEnable()) {
		return;
	}
	DFEngagementLineItemTriggerHandler handler = new DFEngagementLineItemTriggerHandler();
	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			handler.onBeforeInsert(trigger.new);
		} else if (Trigger.isUpdate) {
			DFEngagementLineItemTriggerHandler.increaseBRCount();
			if (DFEngagementLineItemTriggerHandler.isBTriggerActive()) {
				handler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			handler.onBeforeDelete(trigger.old);
		}
	} else if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			handler.onAfterInsert(trigger.newMap);
		} else if (Trigger.isUpdate) {
			DFEngagementLineItemTriggerHandler.increaseARCount();
			if (DFEngagementLineItemTriggerHandler.isATriggerActive()) {
				handler.onAfterUpdate(trigger.newMap, trigger.oldMap);
			}
		} else if (Trigger.isDelete) {
			handler.onAfterDelete(trigger.old);
		}
	}
}