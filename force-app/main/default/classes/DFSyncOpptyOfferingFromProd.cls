public inherited sharing class DFSyncOpptyOfferingFromProd implements Schedulable, Database.Batchable<sObject> {

	public String queryString = null;

	public DFSyncOpptyOfferingFromProd(Set<Id> opptyIds) {
		if (opptyIds != null && !opptyIds.isEmpty()) {
			List<Id> opptyList = new List<Id> (opptyIds);
			queryString = '';
			queryString = 'select id from Opportunity where HasOpportunityLineItem = true  ';
			if (!opptyList.isEmpty()) {
				queryString += ' and id in (\'' + opptyList.get(0) + '\'';
				for (Id opptyId : opptyIds) {
					if (opptyId == null) {
						continue;
					}
					queryString += ',\'' + opptyId + '\'';
				}
				queryString += ')';
			}
		}
		//System.debug('-------------query2---------- :' + queryString);
	}

	public DFSyncOpptyOfferingFromProd() {
		queryString = 'select id from Opportunity where HasOpportunityLineItem = true';
	}

	public void execute(SchedulableContext sc) {
		Integer opptyBatchSize = DFCommonUtils.getOpptyOffProdSyncBatchSize();
		ID batchprocessid = Database.executeBatch(this, opptyBatchSize);
	}

	public Database.QueryLocator start(Database.BatchableContext bC) {
		System.debug('-------------query1---------- :' + queryString);
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext bC, List<Opportunity> opptyList) {
		if (opptyList == null || opptyList.isEmpty()) {
			return;
		}
		Set<Id> opptyIds = new Set<Id> ();

		for (Opportunity oppty : opptyList) {
			if (oppty == null) {
				continue;
			}
			opptyIds.add(oppty.Id);
		}

		String queryStringOli = 'Select id,dFarm__Offering__c from OpportunityLineItem where opportunityid in :opptyIds';

		List<OpportunityLineItem> oliList = Database.query(queryStringOli);

		if (oliList == null || oliList.isEmpty()) {
			return;
		}
		for (OpportunityLineItem oli : oliList) {
			if (oli == null) {
				continue;
			}
			oli.dfarm__Offering__c = null;
		}
		Database.update(oliList, false);
	}

	public void finish(Database.BatchableContext bC) {

	}

	public String createAndStartBatchJob(Set<Id> opptyIds) {
		DFSyncOpptyOfferingFromProd ofmappingJob = new DFSyncOpptyOfferingFromProd(opptyIds);
		Datetime sysTime = System.now().addSeconds(5);
		String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		String jobId = System.schedule('DFSyncOpptyOfferingFromProd_' + chronExpression, chronExpression, ofmappingJob);
		return jobId;
	}


}