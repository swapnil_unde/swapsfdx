public class DFTestData {
    public static User getUser() {
		
		Profile pf= [Select Id from profile where Name='Standard User' limit 1]; 
        Profile p = [select id from profile where Name = :'System Administrator' limit 1];
        
		String orgId=UserInfo.getOrganizationId(); 
       
	    String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        
		Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 

		User userDetails = new User();
		userDetails.FirstName = 'Swapnil';
		userDetails.LastName = 'Unde';
		userDetails.Email = 'swapnil.unde@demandfarm.com';
		userDetails.Username = uniqueName + '@test' + orgId + '.org';
		userDetails.IsActive = true;
		userDetails.Alias = uniqueName.substring(18, 23);
		userDetails.TimeZoneSidKey = 'America/Los_Angeles';
		userDetails.LocaleSidKey = 'en_US';
		userDetails.EmailEncodingKey = 'UTF-8';
		userDetails.ProfileId = pf.Id;
		userDetails.LanguageLocaleKey = 'en_US';

		insert userDetails;
		return userDetails;	
	}

    public static Account getAccount () {
        Account acc = new Account();
        acc.Name = 'Acxiom ' + System.today() + System.now() + Math.random();
        acc.BillingCountry = 'United States';        
        acc.BillingCity = 'Washington';
        acc.BillingState = 'District of Columbia';
        acc.BillingStreet = '';
        acc.BillingPostalCode = '';
        acc.Type= 'Brand';        
        acc.dFarm__IsKeyAccount__c = true;
        if (UserInfo.isMultiCurrencyOrganization()) {
            acc.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
        } 
        insert acc;
        return acc;
    }
    
    public static Opportunity getOpportunity(Id accId,Date closeDate, Date termStart, Date termEnd) {         
        Opportunity opp = new Opportunity();
        opp.Name = 'OWTU Opportunity' + System.today() + + Math.random();
        opp.AccountId = accId;
        opp.StageName = 'Negotiating';
        opp.CloseDate = closeDate; 
        if (UserInfo.isMultiCurrencyOrganization()) {
            opp.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
        }      
        insert opp;
        return opp;
    }
    
    
    public static void addOliToOpportunity(List<Opportunity> opptyList) {
        List<PricebookEntry> pbEntryList = Database.query('Select Id from PricebookEntry');
        if (pbEntryList == null) {
            return;
        }
       
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem> ();
        Integer index = 0;
        for (Opportunity oppty : opptyList) {
            Integer indexVal = Math.mod(index,pbEntryList.size()-1) ;
            PricebookEntry pbData = pbEntryList[indexVal];
            OpportunityLineItem oli = new OpportunityLineItem(UnitPrice = 100, Quantity = 2,PricebookEntryId = pbData.Id, OpportunityId = oppty.Id, ServiceDate = date.today().addMonths(15));
              
            oliList.add(oli);
        	insert oli;       
           index++;
        }
        
    }
    /*
    public static dFArm__Engagement__c getEngagement(Id accId) {
		String defaultOff = 'Systems';
        List<dFarm__Offering__c> offList = Database.query('Select id from dFarm__Offering__c where name = :defaultOff');
        List<dFarm__LineOfBusiness__c> lobList = Database.query('Select Id from dFarm__LineOfBusiness__c');
        
        dFArm__Engagement__c eng = new dFarm__Engagement__c();
        eng.Name = 'testEngg'+ System.today() +  Math.random();
        eng.CurrencyISOCode = 'USD';
        eng.dFarm__Eng_Account__c = accId;
        eng.dFarm__LineOfBusiness__c = lobList[0].Id;
        eng.dFarm__Start_Month__c = date.today();
        eng.dFarm__End_Month__c = date.today().addMonths(15);
        eng.dFarm__Status__c = 'Green';
        eng.dFarm__DFAmount__c = 5000;
        eng.dFarm__Engagement_Offering__c = offList[0].Id;	
        eng.dFarm__PriceBook__c = Test.getStandardPricebookId();
        insert eng;
        return eng;
    }
  
    public static void addEliToEngagement(List<dFarm__Engagement__c> engList) {
        String productC = '7 - Other Software';
        
        List<Product2> products = Database.query('Select Id, CurrencyIsoCode from Product2   where L1__c = null and L2__c = null');
        
        if (products == null) {
            return;
        }
        List<dFarm__EngagementLineItem__c> oliList = new List<dFarm__EngagementLineItem__c> ();
        Integer index = 0;
        for (dFArm__Engagement__C eng : engList) {           
            Product2 pbData = products[0];
            dFarm__EngagementLineItem__c oli = new dFarm__EngagementLineItem__c( CurrencyISOCode = 'USD',dFarm__UnitPrice__C = 100, 
                                                                                dFArm__Quantity__C = 2, dFarm__Product2__c = pbData.Id,
                                                                                dFArm__Engagement__C = eng.Id,
                                                                                dFarm__ServiceDate__c = date.today().addMonths(15));
            oliList.add(oli);
            insert oli;           
        }
        
    }*/

    
    public static void createProdnOfferings() {
        
        dFarm__Offering__c sysOffering = new dFarm__Offering__c();
        sysOffering.Name = 'Self-ServiceTerminal';
        insert sysOffering;
        
        dFarm__Offering__c sysOffering1 = new dFarm__Offering__c();
        sysOffering1.Name = 'DF-Prod1';
        sysOffering1.dFarm__ParentOffering__c = sysOffering.Id; 
        insert sysOffering1;
               
        dFarm__Offering__c sysOffering3 = new dFarm__Offering__c();
        sysOffering3.Name = 'DF-Prod2';
        insert sysOffering3;
        
        Id pbId = Test.getStandardPricebookId();
        List<Product2> prods = new List<Product2> ();
        
        Product2 newProd1 = new Product2( name = 'DF-Prod1', family = 'Self-ServiceTerminal',isActive = true);
        if (UserInfo.isMultiCurrencyOrganization()) {
            newProd1.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
        }
        prods.add(newProd1);
        
        Product2 newProd2 = new Product2( name = 'DF-Prod2', family = 'Self-ServiceTerminal',isActive = true);
        if (UserInfo.isMultiCurrencyOrganization()) {
            newProd2.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
        }
        prods.add(newProd2);
        
        Product2 newProd3 = new Product2(name = 'DF-Prod3', family = 'Self-ServiceTerminal', isActive = true);
        if (UserInfo.isMultiCurrencyOrganization()) {
            newProd3.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
        }
        prods.add(newProd3);        
        
        insert prods;

        List<PricebookEntry> pbEntries = new List<PricebookEntry> ();
        for (Product2 newProd : prods) {
            pricebookentry newPBE = new PriceBookEntry(UnitPrice = 1, Product2Id = newProd.id, Pricebook2Id = pbId, UseStandardPrice = False, ISACTIVE = True);
            if (UserInfo.isMultiCurrencyOrganization()) {
                newPBE.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
            }
            pbEntries.add(newPBE);
            //pricebookentry newPBE1 = new PriceBookEntry(UnitPrice = 1, Product2Id = newProd.id, Pricebook2Id = pbId, UseStandardPrice = False, CurrencyISOCode = 'INR', ISACTIVE = True);
            //pbEntries.add(newPBE1);
        }
        insert pbEntries;
        
    }
    
    public static Contact getContact(Account acc) {  
        Contact con = new Contact();
        con.LastName = 'DFTestCon' + System.today();
        con.AccountID = acc.ID;         
        return con;
    }
    
    public static void createOpptyConRole(Opportunity oppty,Contact con) {        
        OpportunityContactRole opptyConRole = new OpportunityContactRole();
        opptyConRole.IsPrimary = true;
        opptyConRole.ContactId = con.Id;
        opptyConRole.OpportunityId = oppty.Id;
        insert opptyConRole;
    }
    
    public static void setDFSettings() {
        dFArm__DemandFarmSettings__c cstmDemandFarmSettings = DFCommonUtils.getDemandFarmSettings();
        cstmDemandFarmSettings.dFarm__AutoMapBuyingCenterToContact__c = true;
        cstmDemandFarmSettings.dFarm__AutoMapKeyAccountContacts__c = true;
        cstmDemandFarmSettings.dFarm__AutoMapBuyingCenterToOppty__c = false;
        cstmDemandFarmSettings.dFarm__Enable_Auto_Engagement_Creation__c = true;
        cstmDemandFarmSettings.dFarm__Enable_Eng_Split_Edit__c = true;
        cstmDemandFarmSettings.dFarm__Enable_Engagement_Creation_Process__c = false;
        cstmDemandFarmSettings.dFarm__Enable_Oppty_Split_Edit__c = true;
        cstmDemandFarmSettings.dFarm__OpptyStartEndDate__c = 'FromOLI';
        cstmDemandFarmSettings.dFarm__SetOpportunityDefaultDates__c = true;
        cstmDemandFarmSettings.dFarm__EngStartEndDate__c = 'FromOLI';
        cstmDemandFarmSettings.dFarm__ReSyncEngagement__c = false;
        cstmDemandFarmSettings.dFarm__SyncOpptyContactRole__c = true;
        cstmDemandFarmSettings.dFarm__T_And_M_Eng_Splits__c = 'SyncProductLineItems';
        cstmDemandFarmSettings.dFarm__T_AND_M_EngagementOfferingAlgorithm__c = 'FromProduct';
        cstmDemandFarmSettings.dFarm__T_AND_M_OpportunityOfferingAlgorithm__c = 'FromProduct';
        cstmDemandFarmSettings.dFarm__T_And_M_Oppty_Splits__c = 'SyncProductLineItems';
        cstmDemandFarmSettings.dFarm__RollUp_EMS_Amount__c = true;
        cstmDemandFarmSettings.dFarm__csatMaxPermissible__c = 10;
        cstmDemandFarmSettings.dFarm__csatMinPermissible__c = 1;
        //cstmDemandFarmSettings.dFarm__max_Recursion__c = 75;
        
        
        upsert cstmDemandFarmSettings;
        
    }
        
    public static void setDemandFarmSetting() {
        DemandFarmSettings__c dfSetting =  DFCommonUtils.getDFSettings();
        dfSetting.MaxRecursion__c = 4;
        dfSetting.DisableTriggers__c = false;
        dfSetting.DefaultOffering__c = '';
		dfSetting.PrimaryOffering__c = 'Family';
		dfSetting.SecondaryOffering__c = 'Name';
        dfSetting.OpportunityOfferingSyncBatchSize__c = 200;
        upsert dfSetting;
    }
    
    static {      
        setDFSettings();
        setDemandFarmSetting();       
    }
    
}