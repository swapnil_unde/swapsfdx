public inherited sharing class DFOpportunityLineItemTriggerHandler {
	private DFOLIService oliService;

	public DFOpportunityLineItemTriggerHandler() {
		oliService = new DFOLIService();
	}

	public void onBeforeInsert(List<OpportunityLineItem> oliList) {
		List<OpportunityLineItem> oliListToSetOffering = new List<OpportunityLineItem> ();
		for (OpportunityLineItem oli : oliList) {
			if (oli.dFarm__Offering__c == null) {
				oliListToSetOffering.add(oli);
			}
		}
		if (!oliListToSetOffering.isEmpty()) {
			oliService.setOfferingsToOppotyLineItem(oliListToSetOffering);
		}
	}

	public void onAfterInsert(Map<Id, OpportunityLineItem> newOliMap) {
		Set<Id> opptyIds = new Set<Id> ();		
	}

	public void onBeforeUpdate(Map<Id, OpportunityLineItem> newOliMap, Map<Id, OpportunityLineItem> oldOliMap) {
		List<OpportunityLineItem> oliListToSetOffering = new List<OpportunityLineItem> ();
		for (OpportunityLineItem oli : newOliMap.values()) {
			if (oli.dFarm__Offering__c == null) {
				oliListToSetOffering.add(oli);
			}
		}
		if (!oliListToSetOffering.isEmpty()) {
			oliService.setOfferingsToOppotyLineItem(oliListToSetOffering);
		}
	}

	public void onAfterUpdate(Map<Id, OpportunityLineItem> newOliMap, Map<Id, OpportunityLineItem> oldOliMap) {
		Set<Id> opptyIds = new Set<Id> ();		
	}

	public void onBeforeDelete(list<OpportunityLineItem> oldOli) {
		
	}

	public void onAfterDelete(list<OpportunityLineItem> oldOli) {
		
	}


	//recursion
	private static boolean isTriggerDeactivated = false;
	private static Integer recursiveBeforeCallCount = 0;
	private static Integer recursiveAfterCallCount = 0;
	private static Integer maxRecuriveCall = 5;

	static {
		DemandFarmSettings__c dfs = DFCommonUtils.getDFSettings();
		if (dfs != null) {
			maxRecuriveCall = (Integer) dfs.MaxRecursion__c;
		}
		if (maxRecuriveCall == null) {
			maxRecuriveCall = 5;
		}
		resetARCount();
		resetBRCount();
	}

	public static Boolean isBTriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveBeforeCallCount);
		if (recursiveBeforeCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean isATriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveAfterCallCount);
		if (recursiveAfterCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static void increaseBRCount() {
		recursiveBeforeCallCount++;
	}

	public static void resetBRCount() {
		recursiveBeforeCallCount = 0;
	}

	public static void increaseARCount() {
		recursiveAfterCallCount++;
	}

	public static void resetARCount() {
		recursiveAfterCallCount = 0;
	}


	public static void desableTrigger() {
		isTriggerDeactivated = true;
	}

	public static void enableTrigger() {
		isTriggerDeactivated = false;
	}

	public static Boolean isEnable() {
		return !isTriggerDeactivated;
	}
}