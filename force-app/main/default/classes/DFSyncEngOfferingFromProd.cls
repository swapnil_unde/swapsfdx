public inherited sharing class DFSyncEngOfferingFromProd implements Schedulable, Database.Batchable<sObject> {

	public String queryString = null;

	public DFSyncEngOfferingFromProd(Set<Id> engIds) {
		if (engIds != null && !engIds.isEmpty()) {
			List<Id> engList = new List<Id> (engIds);
			queryString = '';
			queryString = 'select id from dFarm__Engagement__c where dFarm__Has_Products__c = true  ';
			if (!engList.isEmpty()) {
				queryString += ' and id in (\'' + engList.get(0) + '\'';
				for (Id engId : engIds) {
					if (engId == null) {
						continue;
					}
					queryString += ',\'' + engId + '\'';
				}
				queryString += ')';
			}
		}
		//System.debug('-------------query2---------- :' + queryString);
	}

	public DFSyncEngOfferingFromProd() {
		queryString = 'select id from dFarm__Engagement__c where dFarm__Has_Products__c = true';
	}

	public void execute(SchedulableContext sc) {
		Integer opptyBatchSize = DFCommonUtils.getOpptyOffProdSyncBatchSize();
		ID batchprocessid = Database.executeBatch(this, opptyBatchSize);
	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('-------------query1---------- :' + queryString);
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext BC, List<dFarm__Engagement__c> engList) {
		if (engList == null || engList.isEmpty()) {
			return;
		}
		Set<Id> engIds = new Set<Id> ();

		for (dFarm__Engagement__c eng : engList) {
			if (eng == null) {
				continue;
			}
			engIds.add(eng.Id);
		}

		String queryStringOli = 'Select id,dFarm__Offering__c from dFarm__EngagementLineItem__c where dFarm__Engagement__c in :engIds';

		List<dFarm__EngagementLineItem__c> eliList = Database.query(queryStringOli);

		if (eliList == null || eliList.isEmpty()) {
			return;
		}
		for (dFarm__EngagementLineItem__c eli : eliList) {
			if (eli == null) {
				continue;
			}
			eli.dfarm__Offering__c = null;
		}
        try {
		Database.update(eliList,false);
        } catch(Exception e) {
            
        }
	}

	public void finish(Database.BatchableContext BC) {

	}

	public String createAndStartBatchJob(Set<Id> engIds) {
		DFSyncEngOfferingFromProd ofmappingJob = new DFSyncEngOfferingFromProd(engIds);
		Datetime sysTime = System.now().addSeconds(5);
		String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		String jobId = System.schedule('DFSyncEngOfferingFromProd_' + chronExpression, chronExpression, ofmappingJob);
		return jobId;
	}
}