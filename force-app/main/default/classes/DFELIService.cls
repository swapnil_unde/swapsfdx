public inherited sharing class DFELIService {

	public void setOfferingsToEngLineItem(List<dFarm__EngagementLineItem__c> eliList) {
		if (eliList == null || eliList.isEmpty()) {
			return;
		}
		Set<Id> prodIds = new Set<Id> ();
		for (dFarm__EngagementLineItem__c eli : eliList) {
			prodIds.add(eli.dFarm__Product2__c);
		}
		Map<Id, Id> prodRelatedOffMap = DFCommonUtils.getProdRelatedOfferingMap(prodIds);
		for (dFarm__EngagementLineItem__c eli : eliList) {
			eli.dFarm__Offering__c = prodRelatedOffMap.get(eli.dFarm__Product2__c);
		}
	}

}