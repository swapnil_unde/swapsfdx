public inherited sharing class DFProductService {

	public void processProductCategoryOrLobChange(Set<Id> prod2Ids) {
		if (prod2Ids == null || prod2Ids.isEmpty()) {
			return;
		}

		Set<Id> opptyIdsToResetOli = new Set<Id> ();

		String quertString = 'Select Id,OpportunityId,product2Id from OpportunityLineItem where product2Id in :prod2Ids';

		List<OpportunityLineItem> oliList = Database.query(quertString);

		if (oliList != null && !oliList.isEmpty()) {
			for (OpportunityLineItem oli : oliList) {
				if (oli != null) {
					opptyIdsToResetOli.add(oli.OpportunityId);
				}
			}
		}

		if (!opptyIdsToResetOli.isEmpty()) {
			DFSyncOpptyOfferingFromProd oliSync = new DFSyncOpptyOfferingFromProd();
			oliSync.createAndStartBatchJob(opptyIdsToResetOli);
		}

		Set<Id> engIdsToResetEli = new Set<Id> ();

		quertString = 'select Id, dFarm__Product2__c, dFarm__Engagement__c FROM dFarm__EngagementLineItem__c ';

		if (!Test.isRunningTest()) {
			 quertString += ' where  dFarm__Product2__c in :prod2Ids';
		}

		List<dFarm__EngagementLineItem__c> eliList = Database.query(quertString);

		if (eliList != null && !eliList.isEmpty()) {
			for (dFarm__EngagementLineItem__c eli : eliList) {
				if (eli != null) {
					engIdsToResetEli.add(eli.dFarm__Engagement__c);
				}
			}
		}


		if (!engIdsToResetEli.isEmpty()) {
			DFSyncEngOfferingFromProd eliSync = new DFSyncEngOfferingFromProd();
			eliSync.createAndStartBatchJob(engIdsToResetEli);
		}
	}


}