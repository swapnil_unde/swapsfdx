public inherited sharing class DFCommonUtils {
	static DemandFarmSettings__c dfs = null;
	static {
		dfs = getDFSettings();
	}

	public static DemandFarmSettings__c getDFSettings() {
		if (dfs != null) {
			return dfs;
		}
		dfs = DemandFarmSettings__c.getInstance(UserInfo.getUserId());
		if (dfs == null) {
			dfs = DemandFarmSettings__c.getInstance(UserInfo.getProfileId());
		}
		if (dfs == null) {
			dfs = DemandFarmSettings__c.getOrgDefaults();
		}
		return dfs;
	}

	public static dFarm__DemandFarmSettings__c getDemandFarmSettings() {
		dFarm__DemandFarmSettings__c dfs = null;
		dfs = dFarm__DemandFarmSettings__c.getInstance(UserInfo.getUserId());
		if (dfs == null) {
			dfs = dFarm__DemandFarmSettings__c.getInstance(UserInfo.getProfileId());
		}
		if (dfs == null) {
			dfs = dFarm__DemandFarmSettings__c.getOrgDefaults();
		}
		return dfs;
	}

	public static Integer getOpptyOffProdSyncBatchSize() {
		DemandFarmSettings__c dfs = getDFSettings();
		Integer opptySize = 1;
		if (dfs != null) {
			try {
				opptySize = (Integer) dfs.OpportunityOfferingSyncBatchSize__c;
			} catch(Exception e) {
			}
		}
		return opptySize;
	}


	public static String getDefaultOfferingName() {
		DemandFarmSettings__c dfs = getDFSettings();
		String offeringName = '';
		if (dfs != null) {
			try {
				offeringName = String.valueOf(dfs.DefaultOffering__c);
			} catch(Exception e) {
			}
		}
		return offeringName;
	}

	public static String getPOfferingApiName() {
		DemandFarmSettings__c dfs = getDFSettings();
		String val = '';
		if (dfs != null) {
			try {
				val = String.valueOf(dfs.PrimaryOffering__c);
			} catch(Exception e) {
			}
		}
		return val;
	}


	public static String getSOfferingApiName() {
		DemandFarmSettings__c dfs = getDFSettings();
		String val = '';
		if (dfs != null) {
			try {
				val = String.valueOf(dfs.SecondaryOffering__c);
			} catch(Exception e) {
			}
		}
		return val;
	}

	public static Map<Id, Id> getProdRelatedOfferingMap(Set<Id> prodIds) {
		Map<Id, Id> prodRelatedOffMap = new Map<Id, Id> ();
		if (prodIds == null || prodIds.isEmpty()) {
			return prodRelatedOffMap;
		}

		String pOfferingApiName = getPOfferingApiName();
		String sOfferingApiName = getSOfferingApiName();

		if ((pOfferingApiName == null || String.isBlank(pOfferingApiName)) && (sOfferingApiName == null || String.isBlank(sOfferingApiName))) {
			return prodRelatedOffMap;
		}

		String queryString = 'SELECT Id,  IsActive  ';

		if (pOfferingApiName != null && !String.isBlank(pOfferingApiName)) {
			queryString += ',' + pOfferingApiName;
		}

		if (sOfferingApiName != null && !String.isBlank(sOfferingApiName)) {
			queryString += ',' + sOfferingApiName;
		}

		queryString += ' FROM Product2 where Id in : prodIds';
		List<Product2> products = Database.query(queryString);
		if (products == null || products.isEmpty()) {
			return prodRelatedOffMap;
		}

		String offeringName = getDefaultOfferingName();
		Set<String> offeringNames = new Set<String> ();
		if (offeringName != null && !String.isBlank(offeringName)) {
			offeringNames.add(offeringName);
		}
		for (Product2 prod : products) {
			if (prod == null) {
				continue;
			}
			if (pOfferingApiName != null && !String.isBlank(pOfferingApiName) && prod.get(pOfferingApiName) != null && !String.isBlank((String) prod.get(pOfferingApiName))) {
				offeringNames.add((String) prod.get(pOfferingApiName));
			}
			if (sOfferingApiName != null && !String.isBlank(sOfferingApiName) && prod.get(sOfferingApiName) != null && !String.isBlank((String) prod.get(sOfferingApiName))) {
				offeringNames.add((String) prod.get(sOfferingApiName));
			}

		}
		if (offeringNames == null || offeringNames.isEmpty()) {
			return prodRelatedOffMap;
		}
		Map<String, Id> offNameIdMap = new Map<String, Id> ();
		queryString = 'select id, name, dFarm__ParentOffering__c, dFarm__ParentOffering__r.Name from dFarm__Offering__c where name in :offeringNames';
		List<dFarm__Offering__c> offerings = Database.query(queryString);
		for (dFarm__Offering__c offering : offerings) {
			String tempOffName = offering.Name;
			if (offering.dFarm__ParentOffering__c != null && !String.isBlank(offering.dFarm__ParentOffering__r.Name)) {
				tempOffName = offering.dFarm__ParentOffering__r.Name + tempOffName;
			}
			offNameIdMap.put(tempOffName, offering.Id);
		}

		for (Product2 prod : products) {
			if (prod == null) {
				continue;
			}

			String prodOffName = null;

			if (pOfferingApiName != null && !String.isBlank(pOfferingApiName) && prod.get(pOfferingApiName) != null && !String.isBlank((String) prod.get(pOfferingApiName))) {
				prodOffName = (String) prod.get(pOfferingApiName);
			}

			if (sOfferingApiName != null && !String.isBlank(sOfferingApiName) && prod.get(sOfferingApiName) != null && !String.isBlank((String) prod.get(sOfferingApiName))) {
				if (prodOffName == null) {
					prodOffName = (String) prod.get(sOfferingApiName);
				} else {
					prodOffName = prodOffName + (String) prod.get(sOfferingApiName);
				}
			}
			if (prodOffName == null && offeringName != null && !String.isBlank(offeringName)) {
				prodOffName = offeringName;
			}

			Id offId = offNameIdMap.get(prodOffName);
			if (offId == null) {
				offId = offNameIdMap.get(offeringName);
			}
			prodRelatedOffMap.put(prod.Id, offId);

		}
		return prodRelatedOffMap;
	}

}