@isTest
public class DFTestOpportunityTrigger {
    
   
    @isTest
    public static void testOpportunityTrigger() {
        Test.startTest();
        DFTestData.createProdnOfferings();
        Account acc = DFTestData.getAccount();
       
        Opportunity oppty1 = DFTestData.getOpportunity(acc.Id,System.today(),null,null);
        oppty1.dFArm__StartMonth__c = System.today();
        oppty1.dFArm__EndMonth__c = System.today();
        update oppty1;
        Opportunity oppty2 = DFTestData.getOpportunity(acc.Id,System.today(),System.today(),System.today());
       	Opportunity oppty3 = DFTestData.getOpportunity(acc.Id,System.today(),System.today(),System.today());
      	Opportunity oppty4 = DFTestData.getOpportunity(acc.Id,System.today(),System.today(),System.today());
        
        List<dFarm__LineOfBusiness__c> lobList = Database.query('Select Id from dFarm__LineOfBusiness__c');
           
        
        
        List<Opportunity> opptyList = new List<Opportunity>();
        opptyList.add(oppty1);
        opptyList.add(oppty2);
        opptyList.add(oppty3);
        opptyList.add(oppty4);
        DFTestData.addOliToOpportunity(opptyList);
                
        List<OpportunityLineItem> oliList = Database.query('select Id from OpportunityLineItem');
        update oliList; 
                
        List<Product2> prods = Database.query('select Id,Name,Family from Product2');
        for (Product2 prod : prods) {
            prod.Name = 'Test 3';           
           // break;
        }       
        update prods;
                
        delete oliList[0];
		        
        delete prods[2];
       
	   Set<Id> opptyIds = new Set<Id>();
	   opptyIds.add(oppty1.Id);
	 
       DFSyncOpptyOfferingFromProd optyProdSer = new DFSyncOpptyOfferingFromProd();
       //optyProdSer.createAndStartBatchJob(opptyIds);     
       Test.stopTest();
    }
    
    
    @isTest
    public static void testOpportunityTriggerSetting() {
        DFCommonUtils.getDemandFarmSettings();
        DFCommonUtils.getDFSettings();
        	        
        	DFOpportunityLineItemTriggerHandler.isATriggerActive();
        	DFOpportunityLineItemTriggerHandler.desableTrigger();
        	DFOpportunityLineItemTriggerHandler.desableTrigger();
        	DFOpportunityLineItemTriggerHandler.increaseARCount();
        	DFOpportunityLineItemTriggerHandler.increaseBRCount();
        	DFOpportunityLineItemTriggerHandler.isATriggerActive();
        	DFOpportunityLineItemTriggerHandler.isBTriggerActive();
        	DFOpportunityLineItemTriggerHandler.isEnable();
        	DFOpportunityLineItemTriggerHandler.resetARCount();
        	DFOpportunityLineItemTriggerHandler.resetBRCount();
        
    }
}