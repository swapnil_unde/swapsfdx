public inherited sharing class DFEngagementLineItemTriggerHandler  {
	private DFELIService eliService;

	public DFEngagementLineItemTriggerHandler() {
		eliService = new DFELIService();
	}

	public void onBeforeInsert(List<dFarm__EngagementLineItem__c> eliList) {
		List<dFarm__EngagementLineItem__c> eliListToSetOffering = new List<dFarm__EngagementLineItem__c> ();
		for (dFarm__EngagementLineItem__c eli : eliList) {
			if (eli.dFarm__Offering__c == null) {
				eliListToSetOffering.add(eli);
			}
		}
		if (!eliListToSetOffering.isEmpty()) {
			eliService.setOfferingsToEngLineItem(eliListToSetOffering);
		}
	}

	public void onAfterInsert(Map<Id, dFarm__EngagementLineItem__c> newEliMap) {
	}

	public void onBeforeUpdate(Map<Id, dFarm__EngagementLineItem__c> newEliMap, Map<Id, dFarm__EngagementLineItem__c> oldEliMap) {
		List<dFarm__EngagementLineItem__c> eliListToSetOffering = new List<dFarm__EngagementLineItem__c> ();
		for (dFarm__EngagementLineItem__c eli : newEliMap.values()) {
			if (eli.dFarm__Offering__c == null) {
				eliListToSetOffering.add(eli);
			}
		}
		if (!eliListToSetOffering.isEmpty()) {
			eliService.setOfferingsToEngLineItem(eliListToSetOffering);
		}
	}

	public void onAfterUpdate(Map<Id, dFarm__EngagementLineItem__c> newEliMap, Map<Id, dFarm__EngagementLineItem__c> oldEliMap) {
	}

	public void onBeforeDelete(list<dFarm__EngagementLineItem__c> oldEli) {
	}

	public void onAfterDelete(list<dFarm__EngagementLineItem__c> oldEli) {
	}


	//recursion
	private static boolean isTriggerDeactivated = false;
	private static Integer recursiveBeforeCallCount = 0;
	private static Integer recursiveAfterCallCount = 0;
	private static Integer maxRecuriveCall = 5;

	static {
		DemandFarmSettings__c dfs = DFCommonUtils.getDFSettings();
		if (dfs != null) {
			maxRecuriveCall = (Integer) dfs.MaxRecursion__c;
		}
		if (maxRecuriveCall == null) {
			maxRecuriveCall = 5;
		}
		resetARCount();
		resetBRCount();
	}

	public static Boolean isBTriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveBeforeCallCount);
		if (recursiveBeforeCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean isATriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveAfterCallCount);
		if (recursiveAfterCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static void increaseBRCount() {
		recursiveBeforeCallCount++;
	}

	public static void resetBRCount() {
		recursiveBeforeCallCount = 0;
	}

	public static void increaseARCount() {
		recursiveAfterCallCount++;
	}

	public static void resetARCount() {
		recursiveAfterCallCount = 0;
	}


	public static void desableTrigger() {
		isTriggerDeactivated = true;
	}

	public static void enableTrigger() {
		isTriggerDeactivated = false;
	}

	public static Boolean isEnable() {
		return !isTriggerDeactivated;
	}
}