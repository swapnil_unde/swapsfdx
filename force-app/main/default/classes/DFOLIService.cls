public inherited sharing class DFOLIService {

	public void setOfferingsToOppotyLineItem(List<OpportunityLineItem> oliList) {
		if (oliList == null || oliList.isEmpty()) {
			return;
		}
		Set<Id> prodIds = new Set<Id> ();
		for (OpportunityLineItem oli : oliList) {
			prodIds.add(oli.Product2Id);
		}
		Map<Id, Id> prodRelatedOffMap = DFCommonUtils.getProdRelatedOfferingMap(prodIds);
		for (OpportunityLineItem oli : oliList) {
			oli.dFarm__Offering__c = prodRelatedOffMap.get(oli.Product2Id);
		}
	}

}