public inherited sharing class DFProductTriggerHandler {
	private DFProductService productService;
	private static DemandFarmSEttings__c dfs;

	public DFProductTriggerHandler() {
		productService = new DFProductService();
	}

	public void onBeforeInsert(list<Product2> productList) {
		if (productList == null || productList.isEmpty()) {
			return;
		}
	}

	public void onAfterInsert(Map<Id, Product2> newProductMap) {
		if (newProductMap == null || newProductMap.isEmpty()) {
			return;
		}
	}

	public void onBeforeUpdate(Map<Id, Product2> newProductMap, Map<Id, Product2> oldProductMap) {
		if (newProductMap == null || newProductMap.isEmpty() || oldProductMap == null || oldProductMap.isEmpty()) {
			//System.debug(' null parameter or trigger is deactive :' + isTriggerActive());
			return;
		}
	}

	public void onAfterUpdate(Map<Id, Product2> newProductMap, Map<Id, Product2> oldProductMap) {
		if (newProductMap == null || newProductMap.isEmpty() || oldProductMap == null || oldProductMap.isEmpty()) {
			return;
		}
		Set<Id> prodIdToResetOli = new Set<Id> ();
		String pOfferingApiName = DFCommonUtils.getPOfferingApiName();
		String sOfferingApiName = DFCommonUtils.getSOfferingApiName();
		if ((pOfferingApiName == null || String.isBlank(pOfferingApiName)) && (sOfferingApiName == null || String.isBlank(sOfferingApiName))) {
			return;
		}
		for (Product2 prod : newProductMap.values()) {
			if (prod != null) {
				Product2 oldProd = oldProductMap.get(prod.Id);
				if (oldProd != null) {
					if (pOfferingApiName != null && !String.isBlank(pOfferingApiName) && prod.get(pOfferingApiName) != oldProd.get(pOfferingApiName)) {
						prodIdToResetOli.add(prod.Id);
					}
					if (sOfferingApiName != null && !String.isBlank(sOfferingApiName) && prod.get(sOfferingApiName) != oldProd.get(sOfferingApiName)) {
						prodIdToResetOli.add(prod.Id);
					}
				}
			}
		}
		if (!prodIdToResetOli.isEmpty()) {
			productService.processProductCategoryOrLobChange(prodIdToResetOli);
		}
	}

	//recursion
	private static boolean isTriggerDeactivated = false;
	private static Integer recursiveBeforeCallCount = 0;
	private static Integer recursiveAfterCallCount = 0;
	private static Integer maxRecuriveCall = 5;

	static {
		dfs = DFCommonUtils.getDFSettings();
		if (dfs != null) {
			maxRecuriveCall = (Integer) dfs.MaxRecursion__c;
		}
		if (maxRecuriveCall == null) {
			maxRecuriveCall = 5;
		}
		resetARCount();
		resetBRCount();
	}

	public static Boolean isBTriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveBeforeCallCount);
		if (recursiveBeforeCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean isATriggerActive() {
		//String CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
		system.debug(' recursiveCallCount :' + recursiveAfterCallCount);
		if (recursiveAfterCallCount <= maxRecuriveCall) {
			return true;
		} else {
			return false;
		}
	}

	public static void increaseBRCount() {
		recursiveBeforeCallCount++;
	}

	public static void resetBRCount() {
		recursiveBeforeCallCount = 0;
	}

	public static void increaseARCount() {
		recursiveAfterCallCount++;
	}

	public static void resetARCount() {
		recursiveAfterCallCount = 0;
	}


	public static void desableTrigger() {
		isTriggerDeactivated = true;
	}

	public static void enableTrigger() {
		isTriggerDeactivated = false;
	}

	public static Boolean isEnable() {
		return !isTriggerDeactivated;
	}

}